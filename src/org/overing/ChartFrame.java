/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.overing;

import java.awt.EventQueue;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.TargetDataLine;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 *
 * @author Overing
 */
public class ChartFrame extends JFrame {

    private static final Logger Log = Logger.getLogger(ChartFrame.class.getSimpleName());
    private static final int PORT = 8086;

    private static final short CMD_HELLO = 0x0001;
    private static final short CMD_TEXT = 0x0002;
    private static final short CMD_AUDIO = 0x0003;
    private static final short CMD_EXIT = (short) 0xFFFF;

    private class RecordTgread extends Thread {

        @Override
        public void run() {
            AudioFormat.Encoding encoding = AudioFormat.Encoding.PCM_SIGNED;
            float sampleRate = 44100f;
            int sampleSize = 16;
            int channels = 2;
            int frameSize = (sampleSize / 8) * channels;
            float frameRate = sampleRate;
            AudioFormat format = new AudioFormat(encoding, sampleRate, sampleSize, channels, frameSize, frameRate, true);
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
            if (!AudioSystem.isLineSupported(info)) {
                JOptionPane.showMessageDialog(ChartFrame.this, "Not audio capture supported", getTitle(), JOptionPane.ERROR_MESSAGE);
                mToggleButton_Audio.setSelected(false);
                mToggleButton_Audio.setEnabled(false);
                return;
            }

            TargetDataLine line;
            try {
                line = (TargetDataLine) AudioSystem.getLine(info);
                line.open(format, line.getBufferSize());
            } catch (Exception ex) {
                Log.log(Level.SEVERE, "Open audio line for capture fail", ex);
                return;
            }
            int frameSizeInBytes = format.getFrameSize();
            int bufferLengthInFrames = line.getBufferSize() / 8;
            int bufferLengthInBytes = bufferLengthInFrames * frameSizeInBytes;
            byte[] data = new byte[bufferLengthInBytes];
            int numBytesRead;

            line.start();

            // TODO write data to mOutput
            while (mToggleButton_Audio.isSelected()) {
                if ((numBytesRead = line.read(data, 0, bufferLengthInBytes)) == -1) {
                    break;
                }
//                out.write(data, 0, numBytesRead);
                try {
                    synchronized (mOutputToken) {
                        mOutput.writeShort(CMD_AUDIO);
                        mOutput.writeInt(numBytesRead);
                        mOutput.write(data, 0, data.length);
                        mOutput.flush();
                    }
                } catch (Exception ex) {
                    Log.log(Level.SEVERE, "Send audio data fail", ex);
                }
            }

            line.stop();
            line.close();
            line = null;
        }
    }

    private class ReadThread extends Thread {

        @Override
        public void run() {
            try {
                while (!isInterrupted()) {
                    short cmd = mInput.readShort();
                    handleCommand(cmd);
                }
            } catch (Exception ex) {
                Log.log(Level.SEVERE, "Read fail", ex);
            }
        }
    }

    private class Server extends Thread {

        @Override
        public void run() {
            mTextArea_Log.append("\nListen client connect");
            ServerSocket server = null;
            try {
                server = new ServerSocket(mPort);
                mSocket = server.accept();
                try {
                    server.close();
                    server = null;
                } catch (IOException igonre) {
                }
                mInput = new DataInputStream(mSocket.getInputStream());
                mOutput = new DataOutputStream(mSocket.getOutputStream());

                mConnectThread = new ReadThread();
                mConnectThread.start();

                mTextField_Message.setEditable(true);
                mToggleButton_Audio.setEnabled(true);

                synchronized (mOutputToken) {
                    mOutput.writeShort(CMD_HELLO);
                    mOutput.flush();
                }
            } catch (Exception ex) {
                Log.log(Level.SEVERE, "Server fail", ex);
            } finally {
                if (server != null) {
                    try {
                        server.close();
                    } catch (IOException igonre) {
                    }
                }
            }
        }
    }

    private class Client extends Thread {

        @Override
        public void run() {
            mTextArea_Log.append("\nConnect to server " + mServerAddr);
            try {
                mSocket = new Socket(mServerAddr, mPort);
                mInput = new DataInputStream(mSocket.getInputStream());
                mOutput = new DataOutputStream(mSocket.getOutputStream());

                mConnectThread = new ReadThread();
                mConnectThread.start();

                mTextField_Message.setEditable(true);
                mToggleButton_Audio.setEnabled(true);
            } catch (Exception ex) {
                Log.log(Level.SEVERE, "Client fail", ex);
            }
        }
    }

    private final Object mOutputToken = new Object();
    private InetAddress mServerAddr;
    private int mPort;
    private Thread mConnectThread;
    private Thread mRecordThread;
    private Socket mSocket;
    private DataInputStream mInput;
    private DataOutputStream mOutput;

    /**
     * Crate chart frame launch servr mode
     *
     * @param serverAddr
     */
    public ChartFrame(int port) {
        mServerAddr = null;
        mPort = port;
        initComponents();
        mConnectThread = new Server();
        mConnectThread.start();
    }

    /**
     * Crate chart frame connect to server address
     *
     * @param serverAddr
     */
    public ChartFrame(InetAddress serverAddr, int port) {
        mServerAddr = serverAddr;
        mPort = port;
        initComponents();
        mConnectThread = new Client();
        mConnectThread.start();
    }

    private void handleCommand(short cmd) throws IOException {
        int length;
        byte[] data;
        switch (cmd) {
            case CMD_HELLO:
                mTextArea_Log.append(String.format("\nReceive {Hello}"));

                if (mServerAddr != null) {
                    synchronized (mOutputToken) {
                        mOutput.writeShort(CMD_HELLO);
                        mOutput.flush();
                    }

                    mTextArea_Log.append("\nReply {Hello}");
                }
                break;

            case CMD_TEXT:
                length = mInput.readInt();
                mInput.read(data = new byte[length]);

                mTextArea_Log.append("\nReceive text: " + new String(data));
                break;

            case CMD_AUDIO:
                length = mInput.readInt();
                mInput.read(data = new byte[length]);

                mTextArea_Log.append("\nReceive audio " + length + " bytes");
                // TODO play audio data
                break;

            case CMD_EXIT:
                mTextArea_Log.append("\nReceive {Exit}");
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        mTextArea_Log = new javax.swing.JTextArea();
        mTextField_Message = new javax.swing.JTextField();
        mToggleButton_Audio = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Chart frame");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        mTextArea_Log.setEditable(false);
        mTextArea_Log.setColumns(20);
        mTextArea_Log.setRows(5);
        jScrollPane1.setViewportView(mTextArea_Log);

        mTextField_Message.setToolTipText("Type message and [Enter] to send");
        mTextField_Message.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mTextField_MessageActionPerformed(evt);
            }
        });

        mToggleButton_Audio.setText("Audio");
        mToggleButton_Audio.setEnabled(false);
        mToggleButton_Audio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mToggleButton_AudioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 561, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(mTextField_Message)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(mToggleButton_Audio)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mTextField_Message, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mToggleButton_Audio))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mTextField_MessageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mTextField_MessageActionPerformed
        String text = evt.getActionCommand();
        if (text == null || text.trim().isEmpty()) {
            return;
        }
        synchronized (mOutputToken) {
            try {
                byte[] data = text.getBytes();
                mOutput.writeShort(CMD_TEXT);
                mOutput.writeInt(data.length);
                mOutput.write(data, 0, data.length);
                mOutput.flush();

                mTextArea_Log.append("\nSend text \"" + text + "\"");
                mTextField_Message.setText("");
            } catch (IOException ex) {
                Log.log(Level.SEVERE, "Send text fail", ex);
            }
        }
    }//GEN-LAST:event_mTextField_MessageActionPerformed

    private void mToggleButton_AudioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mToggleButton_AudioActionPerformed
        JOptionPane.showMessageDialog(this, "施工中...", getTitle(), JOptionPane.WARNING_MESSAGE);
//        if (mToggleButton_Audio.isSelected()) {
//            mRecordThread = new RecordTgread();
//            mRecordThread.start();
//        } else {
//            // TODO 怎中斷? 得想想...
//        }
    }//GEN-LAST:event_mToggleButton_AudioActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try {
            synchronized (mOutputToken) {
                if (mOutput != null) {
                    mOutput.writeShort(CMD_EXIT);
                    mOutput.flush();
                }
            }
        } catch (Exception ex) {
            Log.log(Level.SEVERE, "Send exit fail", ex);
        }
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            Log.log(Level.WARNING, "set look style fail", ex);
        }

        String msg = "Select launch mode:";
        String title = "P2PInterphone";
        Object[] opts = {"Server", "Client"};
        Object mode = JOptionPane.showInputDialog(null, msg, title, JOptionPane.QUESTION_MESSAGE, null, opts, opts[0]);
        if (opts[0].equals(mode)) {
            EventQueue.invokeLater(new Runnable() {

                @Override
                public void run() {
                    new ChartFrame(PORT).setVisible(true);
                }
            });
        } else if (opts[1].equals(mode)) {
            Object ip = JOptionPane.showInputDialog(null, "Server IP:", "127.0.0.1");
            try {
                final InetAddress addr = InetAddress.getByName(ip.toString());
                EventQueue.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        new ChartFrame(addr, PORT).setVisible(true);
                    }
                });
            } catch (Exception ex) {
                Log.log(Level.SEVERE, "Address error", ex);
                msg = String.format("Address error:\n%s", ex.toString());
                JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea mTextArea_Log;
    private javax.swing.JTextField mTextField_Message;
    private javax.swing.JToggleButton mToggleButton_Audio;
    // End of variables declaration//GEN-END:variables
}
